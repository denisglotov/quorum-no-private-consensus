const fs = require('fs');
const net = require('net');
const Web3 = require('web3');

// Actor keys taken from quorum-examples/examples/7nodes/keys/key?
const keys = [
  '0xed9d02e382b34818e88b88a309c7fe71e65f419d',
  '0xca843569e3427144cead5e4d5999a3d0ccf92b8e',
  '0x0fbdc686b912d7722dc86510934589e0aaf3b55a',
];

// Constellation public keys are taken from
// quorum-examples/examples/7nodes/keys/tm?.pub
const constellationKeys = [
  "BULeR8JyUWhiuuCMU/HLA0Q5pzkYT+cHII3ZKBey3Bo=",
  "QfeDAys9MPDs2XHExtc84jKGHxZg/aj52DTh0vtA3Xc=",
  "1iTZde/ndBHvzhcl7V68x44Vx7pl8nwx9LqnM/AfJUg=",
];

// Return a web3 object for the specified node.
function web3({ node }) {
  const port = 22000 + node;
  //const web3 = new Web3(new Web3.providers.HttpProvider(`http://localhost:${port}`));
  console.log(`/home/ubuntu/src/quorum-examples/examples/7nodes/qdata/c${node + 1}/tm.ipc`);
  const web3 = new Web3(new Web3.providers.IpcProvider(
    `/home/ubuntu/src/quorum-examples/examples/7nodes/qdata/c${node + 1}/tm.ipc`,
    net));
  web3.eth.defaultAccount = keys[node];
  return web3;
};

// Return a web3 contract object with the specified `name`.
function contract({ web3, name, address }) {
  const abi = JSON.parse(fs.readFileSync(`build/Demo_sol_${name}.abi`).toString());
  const contract = new web3.eth.Contract(abi, address);
  if (!address) {
    contract.options.data = '0x' + fs.readFileSync(`build/Demo_sol_${name}.bin`).toString();
  }
  return contract;
};

// Deploy the given `contract` with specified `options`.
async function deploy({ contract, options }) {
  const deployed = await contract.deploy()
          .send(options)
          .on('transactionHash', hash => console.log('Tx hash', hash));
  const address =  deployed.options.address;
  contract.options.address = address;
  console.log('Contract deployed at ', address);
  return address;
}

module.exports = { keys, constellationKeys, web3, contract, deploy };
