#!/bin/sh

cd contracts
../node_modules/solc/solcjs *.sol -o ../build --overwrite --optimize --bin --abi
