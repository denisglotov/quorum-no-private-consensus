const utils = require('./utils');

const from = utils.keys[0];
const gas = 500000;

async function show({ name, address, method, nodes }) {
  console.log(await Promise.all(nodes.map(async node => {
    const web3 = utils.web3({ node });
    const contract = utils.contract({ web3, name, address });
    return {
      [method]: (await contract.methods[method]().call({ from })).valueOf(),
    };
  })));
}

async function main() { 
  const web3 = utils.web3({ node: 0 });
  const inner = utils.contract({ web3, name: 'Inner' });
  const outer = utils.contract({ web3, name: 'Outer' });

  const privateFor12 = [ utils.constellationKeys[1] ];
  const privateFor123 = [ utils.constellationKeys[1], utils.constellationKeys[2] ];

  console.log('Deploying inner contract private for nodes 1 and 2...');
  const innerAddress = await utils.deploy(
    { contract: inner, options: { from, gas, privateFor: privateFor12 } });

  console.log('Deploying outer contract private for nodes 1, 2 and 3...');
  const outerAddress = await utils.deploy(
    { contract: outer, options: { from, gas, privateFor: privateFor123 } });
  console.log();

  console.log('Initial Inner states:');
  await show({ name: 'Inner', address: innerAddress, method: 'value', nodes: [0, 1] });
  console.log('Initial Outer states:');
  await show({ name: 'Outer', address: outerAddress, method: 'done', nodes: [0, 1, 2] });
  console.log();
  
  console.log('Invoking outer.call(inner, 42)...');
  await outer.methods.call(innerAddress, 42).send({ from, gas, privateFor: privateFor123 });
  console.log();

  console.log('Resulting Inner states:');
  await show({ name: 'Inner', address: innerAddress, method: 'value', nodes: [0, 1] });
  console.log('Resulting Outer states:');
  await show({ name: 'Outer', address: outerAddress, method: 'done', nodes: [0, 1, 2] });
}

main().catch(console.log);

