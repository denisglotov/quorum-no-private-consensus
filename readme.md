Quorum private transactions have no consensus
=============================================

The problem is that a private transaction in Quorum may pass on some nodes
while it fails (and reverts) on others.

So your backend, sending a private transaction to Quorum network has no way to
know the outcome of the transaction. Even no way to determine if all nodes
fulfilled the transaction or reverted it.

The fact that Quorum nodes may have different private state for the same
contract does not eliminate the problem. A private transaction may change
different states to different sates, this is normal. But having transactions
that pass on some nodes while fail on others is the problem.


How this demo works
-------------------

This demo deploys 2 contracts privately:

* "Outer" contract private for nodes 0, 1 and 2.
* "Inner" contract private for nodes 0 and 1, but not 2.

Then it calls the Outer contract method `call()`, that in turn calls Inner
contract method `notify()`. The transaction succeeds on nodes 0 and 1, while
it fails on node 2.


To run the demo
---------------

This demo works on Quorum 2.0.1 on raft consensus.
You also need `nodejs` version 8 or higher.

1.  Run the 7 nodes Quorum environment
    https://github.com/jpmorganchase/quorum-examples/tree/master/examples/7nodes.

2.  `npm install`

3.  `sh bin/compile.sh`

4.  `node bin/demo.js`
