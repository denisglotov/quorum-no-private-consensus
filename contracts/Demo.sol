pragma solidity ^0.4.18;


contract Inner {
    uint public value = 1;

    function notify(uint val) public {
        value = val;
    }
}


contract Outer {
    uint public done = 1;

    function call(address inner, uint val) public {
        if (inner != address(0)) {
            Inner(inner).notify(val);
        }
        done += 1;
    }
}
